num = int(input("Enter the puzzle number:"))
rows = int(input("Enter the no. of rows:"))
clmns = int(input("Enter the no. of columns:"))
Matrix = []
temp_mat = []
new_mat = []
sort_mat = []
print("Enter the characters:")

for r_elem in range(rows):
    row = []
    for c_elem in range(clmns):
        row.append(input())
    Matrix.append(row)

for r_elem in range(rows):
    for c_elem in range(clmns):
        temp_mat.append(Matrix[r_elem][c_elem])
        print(Matrix[r_elem][c_elem], end = " ")
    print()

def rotations(dir = input("Enter rotations:")):
    curr = temp_mat.index(' ')

    if(dir == 'R'):
        temp = temp_mat[curr]
        temp_mat[curr] = temp_mat[curr+1]
        temp_mat[curr+1] = temp
        return rotations(dir = input("Enter rotations:"))

    elif(dir == 'L'):
        temp = temp_mat[curr]
        temp_mat[curr] = temp_mat[curr-1]
        temp_mat[curr-1] = temp
        return rotations(dir = input("Enter rotations:"))

    elif(dir == 'A'):
        temp = temp_mat[curr]
        temp_mat[curr] = temp_mat[curr-5]
        temp_mat[curr-5] = temp
        return rotations(dir = input("Enter rotations:"))

    elif(dir == 'B'):
        temp = temp_mat[curr]
        temp_mat[curr] = temp_mat[curr+5]
        temp_mat[curr+5] = temp
        return rotations(dir = input("Enter rotations:"))

    elif(dir == 'Z'):
        exit()

    elif(dir == '0'):
        print("--------------------------------------")
        print("Puzzle #", num)
        rand_val = 0
        for r_elem in range(rows):
            nrow = []
            for c_elem in range(clmns):
                nrow.append(temp_mat[rand_val])
                rand_val+=1
            new_mat.append(nrow)
        for r_elem in range(rows):
            for c_elem in range(clmns):
                print(new_mat[r_elem][c_elem], end = " ")
            print()
        return "DONE"


    else:
        return "INVALID rotation"

print(rotations())
